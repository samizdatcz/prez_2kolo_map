var tilesUrl;

if (document.domain == 'localhost') {
	tilesUrl = './prez_2kolo_tiles'
} else {
	tilesUrl = 'https://samizdat.blob.core.windows.net/storage/prez_2kolo_tiles'
};

//tilesUrl = './prez_2kolo_tiles' //odstranit po nahrani na blob

var map = L.map('map', {
		minZoom: 7,
		maxZoom: 13});

function winnerName(value) {
	if (value < 0) {
		return '<span id="karel">K. Schwarzenberg</span>';
	} else {
		return '<span id="milos">M. Zeman</span>';
	}
};

map.scrollWheelZoom.disable();

background = L.tileLayer('https://samizdat.cz/tiles/ton_b1/{z}/{x}/{y}.png', {
	opacity: 1,
    attribution: 'mapová data © přispěvatelé <a target="_blank" href="http://osm.org">OpenStreetMap</a>, obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>'
});

labels = L.tileLayer('https://samizdat.cz/tiles/ton_l2/{z}/{x}/{y}.png', {
	opacity: 1
});

var nehody = L.tileLayer(tilesUrl + '/{z}/{x}/{y}.png', {
	opacity: 1,
});

var utfGrid = new L.UtfGrid(tilesUrl + '/{z}/{x}/{y}.grid.json', {
	useJsonP: false
});

utfGrid.on('mouseover', function(e){ info.update(e);}).on('mouseout', function(e){ info.update();})


var info = L.control();
		info.options.position = 'bottomright';
		info.onAdd = function (map) {
		    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
		    this.update();
		    return this._div;
		};

		info.update = function (props) {
			this._div.innerHTML = (props ?
				'<strong>Obec ' + props.data.nazev_obce + ' v okrese ' + props.data.okres + '</strong><br>'
				+ 'Zvítězil zde <b>' + winnerName(props.data.pcb_delta) + ' o ' 
				+ Math.abs(props.data.hlasy_zema - props.data.hlasy_kniz) + ' hlasů </b>, tedy o ' + Math.round(Math.abs(props.data.pcb_delta) * 10) / 10 + ' procentního bodu.<br>'
				+ 'Miloš Zeman zde získal celkem ' + props.data.hlasy_zema + ' hlasů (' + Math.round((props.data.hlasy_zema / props.data.hlasy) * 1000) / 10 + ' %)'
				: '<b>Najetím myši vyberte obec.</b>');
		};

var form = document.getElementById("frm-geocode");
var geocoder = null;
var geocodeMarker = null;
form.onsubmit = function(event) {
	if (!geocoder) {
		geocoder = new google.maps.Geocoder();
	}
	event.preventDefault();
	var text = document.getElementById("inp-geocode").value;

	geocoder.geocode({'address':text}, function(results, status) {
		if(status !== google.maps.GeocoderStatus.OK) {
			alert("Bohužel, danou adresu nebylo možné najít");
			return;
		}
		var result = results[0];
		var latlng = new L.LatLng(result.geometry.location.lat(), result.geometry.location.lng());
		map.setView(latlng, 15);
	});
};

map.setView([49.7417517, 15.3350758], 8)
			.addLayer(background)
			.addLayer(nehody)
			.addLayer(labels)
			.addLayer(utfGrid)
			.addControl(info)
			